import pyscf
frag = pyscf.M(atom='frag.xyz', basis='ccpvtz')
env  = pyscf.M(atom='env.xyz', basis='sto-3g')
sys = frag + env

def embedding_gradients(sys):
    # Regular HF energy and nuclear gradients of the entire system
    sys_hf = sys.HF().run()
    grad_sys = sys_hf.Gradients().kernel()

    # Construct a CASCI-like effective 1-electron Hamiltonian for the fragment
    # with the presence of outlying atoms in the environment. dm_env is the
    # density matrix in the environment block
    dm_env = sys_hf.make_rdm1()
    dm_env[frag.nao:,:] = dm_env[:,frag.nao:] = 0
    frag_hcore_eff = (sys_hf.get_hcore() + sys_hf.get_veff(sys, dm_env))[:frag.nao, :frag.nao]

    # Customize the zeroth order calculation by overwriting the core Hamiltonian.
    # HF and CISD now provide the embedding wavefunction on fragment.
    geom_frag = sys.atom_coords(unit='Angstrom')[:frag.natm]
    frag.set_geom_(geom_frag)
    frag_hf = frag.HF()
    frag_hf.get_hcore = lambda *args: frag_hcore_eff
    frag_hf.run()()
    frag_ci = frag_hf.CISD().run()

    # The .Gradients() method enables a regular analytical nuclear gradient object
    # to evaluate the Hellmann-Feynman forces on fragment using the first order
    # derivatives of the original fragment Hamiltonian and the variational
    # embedding wavefunction.
    grad_hf_frag = frag_hf.Gradients().kernel()
    grad_ci_frag = frag_ci.Gradients().kernel()

    # Approximate the energy and gradients of the entire system with the post-HF
    # correction on fragment
    approx_e = sys_hf.e_tot + frag_ci.e_tot - frag_hf.e_tot
    approx_grad = grad_sys
    approx_grad[:frag.natm] += grad_ci - grad_hf
    print('Approximate gradients:\n', approx_grad)
    return approx_e, approx_grad

new_sys = pyscf.geomopt.as_pyscf_method(sys,\
          embedding_gradients).Gradients().optimizer().kernel()
