\documentclass[11pt]{article}
\usepackage{csquotes}

\usepackage{xcolor}

\newcommand{\red}[1]{{\color[rgb]{1.0,0,0}{#1}}}
\newcommand{\Rom}[1]{\MakeUppercase{\romannumeral #1}}

\begin{document}
Referee \#1:

\begin{quote}\itshape
This manuscript reviews the design philosophy, implemented methods, and 
software ecosystem the Python Simulations of Chemistry Framework 
(PySCF). The common framework for molecular and extended materials 
provided by PySCF is explained. Particular emphasis is given to how 
the PySCF API enables fast development of new methods by three case 
studies which include code snippets. Examples of applications include 
DMRG calculations of an iron porphyrin and chromium dimer, periodic MP2 
calculations for MgO, G0W0 band structures and CCSD spin densities for 
NiO, as well as embedding calculation. 

This is a useful overview of PySCF's design and capabilities. The 
included code snippets nicely illustrate critical PySCF design 
features and its user-friendly API. 

Parts of the paper are similar to a prior, relatively recent review 
(Ref. 1). It would be helpful to delineate the scope of the present 
paper relative Ref. 1 more clearly. 
\end{quote}

\textbf{Response}:
PySCF is a very active project. A large amount of progress has been made for both molecular
and crystal methods since the last review 3 years ago. As stated in the
introduction, we would like to clarify in this review the thinking behind the development of this package,
illustrate how PySCF can be used as a development platform,
the technology behind the common framework for molecular and material methods,
the transition of the PySCF program from a single group project to a community project,
and its uptake and adoption in fields outside of the chemistry community.
Some newly developed features, relevant applications, and computational performance are briefly
discussed in this paper.


\begin{quote}\itshape
A major shortcoming is that performance "estimates" (Table I) appear to 
be coming out of thin air and seem rather generous when compared to 
applications discussed later. For example, it is claimed that HF 
calculations are possible for 10,000 AOs on a single SMP node with 128 
GB memory without density fitting. Where is this number coming from and 
what does it mean? Does such a calculation take seconds, hours, days, or 
weeks? Are we talking about contracted AOs? Do these numbers refer to model 
Hamiltonians, linear hydrogen chains with minimal basis sets, or more 
realistic applications? In Ref. 1, the "estimate" for HF 
calculations was 5,000 AOs, so what has changed? These "estimates" need 
to be backed up by timings actual calculations, with published 
computational details such as basis sets and convergence criteria and 
version information, especially in the light of statements such as 
"PySCF achieves leading performance in many simulations". 
\end{quote}

\textbf{Response}: We have added Sec.~\Rom{5}~C and Tables~\Rom{2}, \Rom{3} and \Rom{4} 
that support the estimated system sizes that each method can deal with.
However, we cannot provide timing data for every method and 
we do not prohibit users from timing the code themselves.
Since performance can vary depending on available hardware, the numbers we provide
do not represent an attempt to push the boundaries of computation. Instead, they provide a rough estimate about the capabilities
and performance of the package in routine calculations for users with relatively 
``standard'' computation resources.

\begin{quote}\itshape
While modularity and ease of use are important (although 
by new means unique) features for developers, they also introduce 
challenges. For example, how are replicability and reproducibility of 
results ensured? Does PySCF use unit testing? Are there built-in 
consistency checks that will prevent inexperienced developers from 
producing incorrect results? (The paper is relatively silent 
on challenges, even though the vision statement for the special topic 
calls for a discussion of them.) 
\end{quote}

\textbf{Response}:
Quality assurance in general is a challenge in modern software development. At the most basic
level, PySCF provides extensive unit tests for each module to prevent
developers from breaking developed methods when making changes to the
package. Whether or not the PySCF unit tests are enough to ensure reproducibility
is beyond the scope of the current paper. We now discuss these points as part of the long term goals of
future package development in the conclusion: ``\red{Beyond feature development, we will expand our efforts in documentation and in quality assurance and testing.}''

\begin{quote}\itshape
In conclusion, the paper is generally suitable for the JCP special topic 
on electronic structure software, but should be significantly revised 
to address the aforementioned major and the following minor points prior 
to acceptance. 

1. According to Tab. I, symmetry is not implemented for solids, yet it 
is discussed under "new functionality" in Sec. III. 
\end{quote}

\textbf{Response}: In addition to translational symmetry, 
PySCF currently has experimental support for
additional crystal symmetries for solids, as mentioned in Sec. \Rom{3}.
To clarify this, we added a footnote \red{g} in Table \Rom{1} as 
``\red{Experimental support for point-group and time-reversal symmetries in crystals at the SCF and MP2 levels.}''


\begin{quote}\itshape
2. It should be explained how the example in Fig. 5 goes beyond similar 
customization possible through the underlying LIBXC or XCFUN libraries. 
\end{quote}

\textbf{Response}: 
The DFT functional customization in PySCF is not just for functional evaluation
on grids in real space. It integrates functional customization with the
integral evaluation in the Fock build. Thus one can manipulate exact exchange and
attenuated Coulomb interactions in the PySCF DFT functional customization.

\begin{quote}\itshape
3. Additional information on the OpenMP parallelization (which appears 
to be the default SMP parallelization) could be included, e.g., in 
Sec. II. 
\end{quote}

\textbf{Response}: In the last paragraph of Sec.~\Rom{2}, we added ``... achieve thread-level parallelism \red{via OpenMP}, ...''.

\begin{quote}\itshape
4. The color coding of the atoms in Fig. 13 should be defined. What is 
P-cluster? 
\end{quote}

\textbf{Response}: We have defined the color coding by adding ``\red{Fe, orange; S, yellow; C, cyan; O, red; N, blue; H, white; Si, pink.}'' in the caption.
We have also explained the meaning of P-cluster by adding 
``\red{(the [Fe$_8$S$_7$] cluster of nitrogenase)}''.


\begin{quote}\itshape
5. References and acronyms: 

a) References to the PySCF, pip and conda repositories should be 
added. 
\end{quote}

\textbf{Response}: We have added references 2--4 in the first paragraph as 
``... PySCF has been a free and open-source package hosted on Github,\red{$^2$} and is now also available
through pip,\red{$^3$} conda,\red{$^4$}...''

\begin{quote}\itshape
b) Several statements in the introduction should be backed up by 
additional references, "Beyond chemistry and materials science, it 
has also found use in the areas of data science, machine learning, 
and quantum computing, both in academia as well as in industry." 
(p. 2) or "While the fields of quantum chemistry and solid-state 
electronic structure are rich with excellent software [...]." 
(p. 2). 
\end{quote}

\textbf{Response}: 
We have added references 5--16 in the first paragraph as 
``... it has also found use in the areas of data science,\red{$^{5,6}$}
machine learning,\red{$^{7-13}$}
and quantum computing,\red{$^{14-16}$}...''
We have added Refs.~18--25 in the second paragraph.

\begin{quote}\itshape
c) The PySCF home page could be cited as a source for the 
composition of the board of directors. 
\end{quote}

\textbf{Response}: We have added Ref.~17 at the end of the first paragraph.

\begin{quote}\itshape
d) A reference to C. van Wüllen, J. Comput. Chem. 23, 779-785 (2002) 
should be added to the noncollinear spin references in Sec. V A. 
\end{quote}

\textbf{Response}: We have added the suggested reference as Ref.~38 in Sec.~\Rom{5}~A.

\begin{quote}\itshape
e) Please include a reference for EOM-CCSD* in Sec. V B 1. 
\end{quote}

\textbf{Response}: We have added Refs.~75 and 76 in Sec.~\Rom{5}~B~1.

\begin{quote}\itshape
f) Tab. I and Sec. V B 1 mention external active-space solvers, which 
should be named and accompanied by the corresponding references. 
\end{quote}

\textbf{Response}: The external solvers were introduced in Sec.~\Rom{4}~B.
In Table \Rom{1}, we refer the readers to Sec.~\Rom{4}~B by adding ``\red{(as listed in Section \Rom{4}~B)}'' in the footnote e. In Sec.~\Rom{5}~B~1, we have the words 
``As discussed in section \Rom{4}~B'', which points the readers to Sec.~\Rom{4}~B.

\begin{quote}\itshape
g) The acronym X2C is first used in Sec. V C on page 11 but not 
defined. While Ref. 102 derived the correct renormalization 
matrix, the X2C method was introduced by W. Kutzelnigg and W. Liu, 
J. Chem. Phys. 123, 241102 (2005), W. Liu and D. Peng, 
J. Chem. Phys. 125, 044102 (2006), and M. Iliaš and T. Saue, 
J. Chem. Phys. 126, 064102 (2007). 
\end{quote}

\textbf{Response}: We have defined X2C as ``\red{exact-two-component (X2C)}'' in Sec. \Rom{5}~C and added the suggested references as Refs.~111--114.

\begin{quote}\itshape
h) The references for gauge including atomic orbitals (Sec. V C, page 11) 
should be replaced or complemented by F. London, J. Phys. Radium 
8, 397-409 (1937) and R. Ditchfield, Mol. Phys. 27, 789-807 (1974). 
\end{quote}

\textbf{Response}: We have added the suggested references in Sec.~\Rom{5}~C as Refs.~106 and 107.

\begin{quote}\itshape
i) Please define the acronyms TDA, TDDFT. 
\end{quote}

\textbf{Response}: In Sec.~\Rom{5}~C, we have changed the text to
``... analytical gradients of \red{time-dependent density functional theory (TDDFT) with or without the Tamm-Dancoff approximation (TDA)}
for excited state geometry optimization.''

\begin{quote}\itshape
6. A license file for Fig. 8 appears to be missing. Moreover, 
Figs. 7, 11, and 13 may require licenses. 
\end{quote}

\textbf{Response}: Figs.~7 and 11 do not require licenses. The copyright of Fig.~13 belongs to the original authors of the article, who are the co-authors of this work. We have attached the license file for Fig.~8.


\begin{quote}\itshape
Typos etc.: 

1. Is there a corresponding author? 

2. Some affiliations contain inconsistencies, e.g., 
4: include space in "Sciences,Beijing" 
13: Missing country 
16, 17, 21, 30, 39: Abbreviate states 
31: Postal code and street address missing 

3. Refs. 1, 22, 44, 139, 140: Inconsistent abbreviation of "Wiley 
Interdisciplinary Reviews: Computational Molecular Science". 

4. Ref. 4: "G. K.-l. Chan" should probably be "G. K.-L. Chan". Similar 
issue in Ref. 24. 

5. According to the AIP style guide, all co-authors must be named in 
references. Please update Refs. 1, 2, 48, 50, 88, 96, 100, 112, 113, 
115, 125, 137, 139, 145, 146, 150, and 151. 
\end{quote}

\textbf{Response}: We have fixed these issues.

\clearpage


Referee \#2:

\begin{quote}\itshape
This article is very well written, and I enjoyed reading it. It provides a nice summary of the capabilities provided by PySCF. It also provides compelling arguments showing that PySCF is a nice environment for development. I especially enjoyed reading about the goals and design philosophy of the project, and how with a little up-front planning it is possible to support both molecules and periodic computations. This is apparently easier than I would have thought (although by no means trivial), and the discussion of this issue was very informative.

I am happy to recommend publication in JCP. I suggest a few minor edits.

Table I, footnote e: presumably there are a small number of interfaced options. It might be nice to explicitly list them here or point to where the list can be found in the text
\end{quote}

\textbf{Response}: We now refer readers to Sec.~\Rom{4}~B by adding ``\red{(as listed in Section \Rom{4}~B)}'' in footnote e of Table \Rom{1}.

\begin{quote}\itshape
Page 10, bottom of first column: please provide references for the IP-EOM-CCSD* and EA-EOM-CCSD* methods
\end{quote}

\textbf{Response}: We have added Refs.~75 and 76 in Sec.~\Rom{5}~B~1.

\begin{quote}\itshape
Page 11: Please move the reference to X2C up a few lines to the first time X2C is mentioned 
\end{quote}

\textbf{Response}: We have made the change.

\begin{quote}\itshape
The large number of orbital localization methods provided is a nice feature of the code

Page 13: It is clear that MPI parallelism is handled in an unconventional way. However, I do not understand what ``functions and data'' are sent to the different MPI processes. Could the authors elaborate in just a few more sentences about how the data and tasks are distributed? For example, in an SCF, is each MPI process building a partial Fock matrix based on a subset of all the 3-index DF integrals, or what? (I appreciate that to get the full understanding I could read reference 1, but it seems helpful to have just a bit more in the current paper).
\end{quote}

\textbf{Response}: 
In section V.H, we added some explanation using the example of Fock-build:
``\red{For example, when building the Fock matrix in the PySCF MPI implementation,
	the Fock-build function running on the master process first sends itself to
	the Python interpreters running on the clients.
	After the function is decoded on the clients, input variables (like the
	density matrix) are distributed by the master process through MPI. Each client
	evaluates a subset of the four-center two-electron integrals (with load balancing
	performed among the clients) and constructs a partial Fock matrix, similarly to the Fock-build
	functions in other MPI implementations. After sending the partial Fock matrices back to
	the master process, the client suspends itself until it receives the next function.
	The master process assembles the Fock matrices and then moves on to the next part of
	the code.}''

\begin{quote}\itshape
Page 14: ``CPPE: This library provides a polarizable embedding solvent model and can be integrated into PYSCF calculations of excited states''. Presumably the authors mean ground or excited states?
\end{quote}

\textbf{Response}: We have made it clearer by rephrasing the last sentence in Sec.~\Rom{6}~A as 
``... integrated into PySCF calculations for
\red{ground-state mean-field and post-SCF methods. In addition, an interface to TDA is currently supported for excited-state calculations.}''



\end{document}
