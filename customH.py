import numpy
import pyscf
mol = pyscf.M()
n = 10
mol.nelectron = n

# Define model Hamiltonian: tight binding on a ring
h1 = numpy.zeros((n, n))
for i in range(n-1):
    h1[i, i+1] = h1[i+1, i] = -1.
h1[n-1, 0] = h1[0, n-1] = -1.

# Build the 2-electron interaction tensor starting from a random 3-index tensor.
tensor = numpy.random.rand(2, n, n)
tensor = tensor + tensor.transpose(0, 2, 1)
eri = numpy.einsum('xpq,xrs->pqrs', tensor, tensor)

# SCF for the custom Hamiltonian
mf = mol.HF()
mf.get_hcore = lambda *args: h1
mf.get_ovlp = lambda *args: numpy.eye(n)

# Option 1: overwrite the attribute mf._eri for the 2-electron interactions
mf._eri = eri
mf.run()

# Option 2: introduce the 2-electron interaction through the Cholesky decomposed tensor.
dfmf = mf.density_fit()
dfmf.with_df._cderi = tensor
dfmf.run()

# Option 3: define a custom HF potential method
def get_veff(mol, dm):
    J = numpy.einsum('xpq,xrs,pq->rs', tensor, tensor, dm)
    K = numpy.einsum('xpq,xrs,qr->ps', tensor, tensor, dm)
    return J - K * .5
mf.get_veff = get_veff
mf.run()

# Call the second order SCF solver in case converging the DIIS-driven HF method
# without a proper initial guess is difficult.
mf = mf.newton().run()

# Run post-HF methods based on the custom SCF object
mf.MP2().run()
mf.CISD().run()
mf.CCSD().run()
mf.CASSCF(4, 4).run()
mf.CASCI(4, 4).run().NEVPT2().run()
mf.TDHF().run()
mf.CCSD().run().EOMIP().run()
mc = shci.SHCISCF(mf, 4, 4).run()
mc = dmrgscf.DMRGSCF(mf, 4, 4).run()
