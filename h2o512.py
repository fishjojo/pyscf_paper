import numpy
from matplotlib import pyplot as plt

# 512 H2O molecules
dat = numpy.array([
# cores    wall time
[32//32  , 24576.24],
[64//32  , 15190.34],
[128//32 ,  7626.23],
[256//32 ,  3848.86],
[512//32 ,  1954.41],
[1024//32,  1008.82],
[2048//32,   559.31], ])

def show_result(af1, filename):
    fig = af1.figure
    #plt.show()
    fig.savefig(filename+'.eps', format='eps')
    fig.savefig(filename+'.pdf', format='pdf')

def plot_mpi():
    fig = plt.figure()#figsize=(8,5))
    af1 = fig.add_subplot(111)
    af2 = af1.twinx()  # instantiate a second axes that shares the same x-axis
    ncpu = dat[:,0]
    time = dat[:,1]
    speedup = time[0] / time
    speedup_ideal = ncpu / ncpu[0]

    af1.plot(ncpu, time, color='k', ls='-')
    af1.set_ylabel('Wall time (s)')
    af1.set_xlabel('Number of MPI processes')
    af1.set_xlim(0, 2048//32)

    af2.plot(ncpu, speedup, color='k', ls='-')
    af2.plot(ncpu, speedup_ideal, color='k', ls='--')
    af2.set_ylabel('speed up')
    show_result(af1, 'h2o512_mpi')

if __name__ == '__main__':
    plot_mpi()
